class Scrabble
  @@points_hash = {
    "A" => 1, "B" => 3, "C" => 3, "D" => 2,
    "E" => 1, "F" => 4, "G" => 2, "H" => 4,
    "I" => 1, "J" => 8, "K" => 5, "L" => 1,
    "M" => 3, "N" => 1, "O" => 1, "P" => 3,
    "Q" => 10, "R" => 1, "S" => 1, "T" => 1,
    "U" => 1, "V" => 4, "W" => 4, "X" => 8,
    "Y" => 4, "Z" => 10
  }

  def score(str = "", options = {})
    return 0 unless str
    total_points = 0
    str.each_char do |chr|
      points = @@points_hash[chr.upcase]
      total_points += points if points
    end

    total_points = apply_single_letter_multipliers(total_points, options)
    apply_word_multipliers(total_points, options)
  end

  def apply_word_multipliers(points, options)
    if (options[:double_word] && options[:double_word] > 0)
      points *= 2 ** options[:double_word]
    end
    if (options[:triple_word] && options[:triple_word] > 0)
      points *= 3 ** options[:triple_word]
    end
    points
  end

  def apply_single_letter_multipliers(points, options)
    single_letters = options[:single_letters]
    if (single_letters)
      single_letters.each {|letter, multiplier| points += @@points_hash[letter] * (multiplier-1) }
    end
    points
  end

end
