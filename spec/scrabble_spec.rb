require 'Scrabble'

RSpec.describe Scrabble, "#score" do
  it "should return 0 with an empty string" do
    game = Scrabble.new
    expect(game.score("")).to eq 0
  end

  it "should return 0 when nil is passed in" do
    game = Scrabble.new
    expect(game.score(nil)).to eq 0
  end

  it "should return 8 when 'hello' is passed in" do
    game = Scrabble.new
    expect(game.score("hello")).to eq 8
  end

  it "should ignore invalid characters" do
    game = Scrabble.new
    expect(game.score("hel_lo")).to eq 8
  end

  it "should return 16 when 'hello' is passed in with a double word score" do
    game = Scrabble.new
    expect(game.score("hello", { double_word: 1 })).to eq 16
  end

  it "should return 24 when 'hello' is passed in with a triple word score" do
    game = Scrabble.new
    expect(game.score("hello", { triple_word: 1 })).to eq 24
  end

  it "should return 32 when 'hello' is passed in with two double word scores" do
    game = Scrabble.new
    expect(game.score("hello", { double_word: 2 })).to eq 32
  end

  it "should return 72 when 'hello' is passed in with two triple word scores" do
    game = Scrabble.new
    expect(game.score("hello", { triple_word: 2 })).to eq 72
  end

  it "should return 12 when 'hello' is passed in with 'H' on a double letter score" do
    game = Scrabble.new
    expect(game.score("hello", { single_letters: {"H" => 2} })).to eq 12
  end

  it "should return 16 when 'hello' is passed in with 'H' on a double letter score" do
    game = Scrabble.new
    expect(game.score("hello", { single_letters: {"H" => 3} })).to eq 16
  end

  it "should return 24 when 'hello' is passed in with 'H' on a double letter score and has a double word score" do
    game = Scrabble.new
    expect(game.score("hello", { double_word: 1, single_letters: {"H" => 2} })).to eq 24
  end
end
